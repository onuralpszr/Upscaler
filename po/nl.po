# Dutch translations for upscaler package.
# Copyright (C) 2022 THE upscaler'S COPYRIGHT HOLDER
# This file is distributed under the same license as the upscaler package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: upscaler\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-21 13:54+0100\n"
"PO-Revision-Date: 2022-11-21 13:56+0100\n"
"Last-Translator: Philip Goto <philip.goto@gmail.com>\n"
"Language-Team: none\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: data/io.gitlab.theevilskeleton.Upscaler.desktop.in:3
#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:5
#: upscaler/gtk/window.blp:9 upscaler/gtk/window.blp:33
msgid "Upscaler"
msgstr "Upscaler"

#: data/io.gitlab.theevilskeleton.Upscaler.desktop.in:4
msgid "Upscale"
msgstr "Opschalen"

#: data/io.gitlab.theevilskeleton.Upscaler.desktop.in:5
#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:7
msgid "Upscale and enhance images"
msgstr "Schaal afbeeldingen op en verbeter deze"

#: data/io.gitlab.theevilskeleton.Upscaler.desktop.in:11
msgid "image;upscale;upscaling;processing"
msgstr "image;upscale;upscaling;processing;afbeelding;opschalen;verwerking;"

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:6
msgid "Hari Rana"
msgstr "Hari Rana"

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:13
msgid ""
"Upscaler allows you to upscale and enhance a given image. It is a front-end "
"for Real-ESRGAN ncnn Vulkan."
msgstr ""
"Met Upscaler kunt u een gegeven afbeelding opschalen en verbeteren. Het is "
"een front-end voor ‘Real-ESRGAN ncnn Vulkan’."

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:45
msgid "Overview"
msgstr "Overzicht"

#: data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in:65
msgid "Initial Upscaler release"
msgstr "Eerste uitgave van Upscaler"

#: upscaler/file_chooser.py:93
msgid "Select an image"
msgstr "Afbeelding selecteren"

#: upscaler/file_chooser.py:117
msgid "No file extension was specified"
msgstr "Er is geen bestandsextensie opgegeven"

#: upscaler/file_chooser.py:122
msgid "’{}’ is an unsupported format"
msgstr "‘{}’ is een niet-ondersteund formaat"

#: upscaler/file_chooser.py:135
msgid "Select output location"
msgstr "Uitvoerlocatie selecteren"

#: upscaler/filters.py:41
msgid "Supported image files"
msgstr "Ondersteunde afbeeldingsbestanden"

#: upscaler/gtk/dialog-upscaling.blp:20
msgid "Upscaling…"
msgstr "Opschalen…"

#: upscaler/gtk/dialog-upscaling.blp:21
msgid "This could take a while."
msgstr "Dit kan even duren."

#: upscaler/gtk/dialog-upscaling.blp:25
msgid "Loading…"
msgstr "Aan het laden…"

#: upscaler/gtk/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr "Algemeen"

#: upscaler/gtk/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Sneltoetsen tonen"

#: upscaler/gtk/help-overlay.blp:19
msgctxt "shortcut window"
msgid "Quit"
msgstr "Sluiten"

#: upscaler/gtk/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Open Image"
msgstr "Afbeelding openen"

#: upscaler/gtk/window.blp:40
msgid "_Open Image…"
msgstr "Afbeelding _openen…"

#: upscaler/gtk/window.blp:77
msgid "Properties"
msgstr "Eigenschappen"

#: upscaler/gtk/window.blp:80
msgid "Image Size"
msgstr "Afbeeldingsgrootte"

#: upscaler/gtk/window.blp:88
msgid "Post-upscale Image Size"
msgstr "Afbeeldingsgrootte na opschalen"

#: upscaler/gtk/window.blp:89
msgid "Image size after it is upscaled."
msgstr "Afbeeldingsgrootte nadat deze is opgeschaald."

#: upscaler/gtk/window.blp:98
msgid "Options"
msgstr "Opties"

#: upscaler/gtk/window.blp:122
msgid "Type of Image"
msgstr "Afbeeldingstype"

#: upscaler/gtk/window.blp:130
msgid "Save Location"
msgstr "Opslaglocatie"

#: upscaler/gtk/window.blp:144
msgid "(None)"
msgstr "(geen)"

#: upscaler/gtk/window.blp:155
msgid "_Upscale"
msgstr "_Opschalen"

#: upscaler/gtk/window.blp:156
msgid "Save location is missing."
msgstr "Opslaglocatie ontbreekt."

#: upscaler/gtk/window.blp:176
msgid "Could not Load Image"
msgstr "Kon afbeelding niet laden"

#: upscaler/gtk/window.blp:177
msgid "This image could be corrupted or may use an unsupported file format."
msgstr ""
"Deze afbeelding kan beschadigd zijn of een niet-ondersteund bestandsformaat "
"gebruiken."

#: upscaler/gtk/window.blp:197
msgid "Open File…"
msgstr "Bestand openen…"

#: upscaler/gtk/window.blp:209
msgid "Keyboard Shortcuts"
msgstr "Sneltoetsen"

#: upscaler/gtk/window.blp:214
msgid "About Upscaler"
msgstr "Over Upscaler"

#: upscaler/main.py:69
msgid "Algorithms by"
msgstr "Algoritmes door"

#: upscaler/main.py:76
msgid "Code and Design Borrowed from"
msgstr "Gebaseerd op code en ontwerp door"

#: upscaler/main.py:85
msgid "Sample Image from"
msgstr "Voorbeeldafbeelding door"

#: upscaler/main.py:124
msgid ""
"Jürgen Benvenuti <gastornis@posteo.org>\n"
"Philip Goto <philip.goto@gmail.com>\n"
"Sabri Ünal <libreajans@gmail.com>\n"
"yukidream https://fosstodon.org/@yukidream\n"
"Anatoly Bogomolov <tolya.bogomolov2019@gmail.com>"
msgstr ""
"Jürgen Benvenuti <gastornis@posteo.org>\n"
"Philip Goto <philip.goto@gmail.com>\n"
"Sabri Ünal <libreajans@gmail.com>\n"
"yukidream https://fosstodon.org/@yukidream\n"
"Anatoly Bogomolov <tolya.bogomolov2019@gmail.com>"

#: upscaler/window.py:55
msgid "Photo"
msgstr "Foto"

#: upscaler/window.py:56
msgid "Cartoon/Anime"
msgstr "Cartoon/Anime"

#: upscaler/window.py:129
msgid "Image upscaled"
msgstr "Afbeelding opgeschaald"

#: upscaler/window.py:130
msgid "Open"
msgstr "Openen"

#: upscaler/window.py:136
msgid "Model name: {}"
msgstr "Modelnaam: {}"

#~ msgid "’{}’ upscaled"
#~ msgstr "‘{}’ opgeschaald"

#~ msgid "Open ’{}’"
#~ msgstr "‘{}’ openen"
